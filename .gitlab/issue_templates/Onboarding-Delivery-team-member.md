## New team member

Welcome to the Delivery team!

The information below is meant to serve as a quick overview of both the company and the team resources.

Feel free to skip parts you are already familiar with, but keep in mind that some of the resources might have changed since you read them so a quick refresher could be useful.

## Company
GitLab is organised based on the product categories.  Also have a look at the team org chart.

## Team
Like all teams we have a Delivery team section in the handbook. Take some time to read through, including all the linked resources.

Team member onboarding tasks
- [ ] Read the [Delivery Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/) handbook section
- [ ] Read the [Platforms Stage](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/) handbook section
- [ ] Review the [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/) handbook section
- [ ] Join the following Slack channels:
   - Delivery related channels
      - [ ] #eng-week-in-review (mandatory channel for engineering)
      - [ ] #g_delivery (team channel)
      - [ ] #s_platforms (platforms stage channel)
      - [ ] #delivery_lounge (Delivery team social chats)
      - [ ] #infrastructure-lounge  (Infrastructure department channels)
      - [ ] #infra-lounge-social (Infrastructure department social chats)
   - Release management channels
      - [ ] #releases
      - [ ] #f_upcoming_release (release management)
      - [ ] #announcements (deployment feed, and general infrastructure announcements)
      - [ ] #production
      - [ ] #incident-management
- [ ] Read the [keeping yourself informed documentation](https://about.gitlab.com/handbook/marketing/strategic-marketing/getting-started/communication/)
- [ ] [Update the team page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page)

## Infrastructure tasks
- [ ] If you're going to be interacting with Kubernetes at all in your role, you'll need to go through the steps in the [k8s on-call setup doc](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md).
  * Alongside this, you'll also need to do two things:
    1. Go through the docs on [access to the bastion hosts](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/bastions) to add relevant sections to your SSH config. Without these configs you won't be able to authenticate with our K8s clusters
    1. Add yourself as a new user to the collection of [Chef data bags](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/tree/master/data_bags/users), providing an SSH public key which will be propagated to the bastions. [Here's a sample MR for reference](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/commit/d9d0d4cd168a4373a9a85fdb8efad743ef3f7e01). Your baseline Access Request that your manager will have created for you also contains a to-do that requires an existing Chef Admin to make your new user an admin, which your manager will be able to help you with.
- [ ] Review the handbook section on the [GitLab Sandbox](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/). This is essentially your portal to individual AWS and GCP accounts to use for development and testing.
  * Go through the steps to [set up a new CSP account](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project) for yourself.
  * If you'll be interacting with Terraform at all in your role, it's also worth reading through the section on [Terraform Environments](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#terraform-environments) for your individual CSP account.
- [ ] Even though Delivery does not develop the product directly, it's worth familiarising yourself with the [basic architecture of GitLab](https://docs.gitlab.com/ee/development/architecture.html), and having a passing familiarity with the [components that make up GitLab](https://docs.gitlab.com/ee/development/architecture.html#component-list). This is by no means required, but it can help to establish some context around the work that Delivery does to facilitate deployment and releases of the platform and its components.

## Manager onboarding tasks
For all team members:
- [ ] Add new team member to delivery-team@gitlab.com Google group
- [ ] Add new team member to [Delivery group](https://gitlab.com/groups/gitlab-org/delivery/-/group_members?with_inherited_permissions=exclude) on GitLab.com
- [ ] Add new team member to [Ops Delivery group](https://ops.gitlab.net/groups/gitlab-com/delivery/-/group_members)
- [ ] Create an access request to add to the delivery-team slack group
- [ ] Create access request to 'Release' and 'Build' vaults
- [ ] Add to weekly team meeting
- [ ] Create a 1-1 document and schedule a recurring weekly meeting

Role specific access requests
- [ ] Create an access request for Managers, Infrastructure baseline entitlement
- [ ] Create an access request for Backend, Infrastructure baseline entitlement
- [ ] Create an access request for SRE, Infrastructure entitlement

## Training resources

You will be given a full Release Manager onboarding before you join the release manager rotation. The links below are for general awareness of what the Delivery team does. 

- [ ] [Release Manager responsibilities](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/index.md#responsibilities)
- [ ] Overall release process can [be found here](https://about.gitlab.com/handbook/engineering/releases). 
- [ ] [Deployments big picture](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/big_picture.md)
- [ ] Most of our work is on [release tools](https://gitlab.com/gitlab-org/release-tools), [k8s-workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com), [Deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer), or [Chatops](https://gitlab.com/gitlab-com/chatops) 
- [ ] We track Delivery work in the [Delivery issue tracker](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues), and release issues in the [Release issue tracker](https://gitlab.com/gitlab-org/release/tasks/issues) 
- [ ] The team current work can be found on our [Board](https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/1918862?label_name[]=Delivery%20team%3A%3ABuild&label_name[]=team%3A%3ADelivery)
- [ ] Work epics for [release velocity](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170), and for [Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)
- [ ] Browse the [release docs and runbooks](https://gitlab.com/gitlab-org/release/docs/#gitlab-release-process)

### Dashboards

* [Dashboard showing resource utilization between virtual machines and the Kubernetes clusters for GitLab.com](https://dashboards.gitlab.net/d/delivery-k8s_migration_overview/delivery-kubernetes-migration-overview?orgId=1&refresh=5m)
* [Release manager dashboard](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/dashboard.md)
* [Omnibus versioning dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m)
* [Kibana logs for release-tools](https://nonprod-log.gitlab.net/goto/1ad82080-5708-11ed-9af2-6131f0ee4ce6)
